package br.com.voffice.microservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootLab01Application {

	public static void main(String[] args) {
		SpringApplication.run(BootLab01Application.class, args);
	}
}
